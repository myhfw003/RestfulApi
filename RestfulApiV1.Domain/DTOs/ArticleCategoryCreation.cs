﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestfulApiV1.Domain.DTOs
{
    public class ArticleCategoryCreation
    {
        public string CategoryName { get; set; }
        public string Description { get; set; }
        public string ParentCategoryId { get; set; }
        public string Remarks { get; set; }
    }
}
