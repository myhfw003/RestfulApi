﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestfulApiV1.Domain.DTOs
{
    public class ArticleCreation
    {
        public string Title { get; set; }
        public string ArticleContent { get; set; }
        public string CoverImgUrl { get; set; }
        public int ArticleStatusId { get; set; }
        public int ArticleCategoryId { get; set; }
    }
}
