﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestfulApiV1.Domain.Configure
{
    public class TokenParameter
    {
        public string Secret { get; set; }
        public string Issuer { get; set; }
        public double AccessExpiration { get; set; }
        public double RefreshExpiration { get; set; }
    }
}
