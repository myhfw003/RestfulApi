﻿using Microsoft.EntityFrameworkCore;
using RestfulApiV1.Data.Entity;

namespace RestfulApiV1.Data
{
    public class RestfulApiDbContext:DbContext
    {
        public RestfulApiDbContext()
        {
        }

        public RestfulApiDbContext(DbContextOptions options):base(options)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //单表树状结构
            modelBuilder.Entity<ArticleCategory>()
                //主语this，拥有Children
                .HasMany(x => x.Children)
                //主语Children，每个Child拥有一个Parent
                .WithOne(x => x.ParentCategory)
                //主语Children，每个Child的外键是ParentId
                .HasForeignKey(x => x.ParentCategoryId)
                //这里必须是非强制关联，否则报错：Specify ON DELETE NO ACTION or ON UPDATE NO ACTION, or modify other FOREIGN KEY constraints.
                .OnDelete(DeleteBehavior.ClientSetNull);

            base.OnModelCreating(modelBuilder);
        }



        public DbSet<Users> Users { get; set; }

        public DbSet<Roles> Roles { get; set; }

        public DbSet<UserRole> UserRoles { get; set; }

        public DbSet<RolePermission> RolePermissions { get; set; }
        public DbSet<ArticleStatus> ArticleStatuses { get; set; }
        public DbSet<Articles> Articles { get; set; }
        public DbSet<ArticleCategory> ArticleCategories { get; set; }
    }
}
