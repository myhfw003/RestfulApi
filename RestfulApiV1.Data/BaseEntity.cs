﻿using System;
namespace RestfulApiV1.Data
{
    public class BaseEntity
    {
        public BaseEntity()
        {
            IsActived = true;
            IsDeleted = false;
            CreatedTime = DateTime.Now;
            UpdatedTime = DateTime.Now;
        }


        public int Id { get; set; }
        public bool IsActived { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedTime { get; set; }
        public DateTime UpdatedTime { get; set; }
        public int DisplayOrder { get; set; }
        public string Remarks { get; set; }

    }
}
