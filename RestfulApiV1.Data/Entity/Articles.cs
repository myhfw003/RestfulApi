﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestfulApiV1.Data.Entity
{
    public class Articles:BaseEntity
    {
        public string Title { get; set; }
        public string ArticleContent { get; set; }
        public string CoverImgUrl { get; set; }
        public int ArticleStatusId { get; set; }
        public virtual ArticleStatus ArticleStatus { get; set; }
        public int ArticleCategoryId { get; set; }
        public virtual ArticleCategory ArticleCategory { get; set; }
    }
}
