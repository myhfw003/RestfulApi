﻿using System.Collections.Generic;

namespace RestfulApiV1.Data.Entity
{
    public class Roles:BaseEntity
    {
        public string RoleName { get; set; }
        public string Description { get; set; }

    }
}