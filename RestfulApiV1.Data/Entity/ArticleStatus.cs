﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestfulApiV1.Data.Entity
{
    public class ArticleStatus:BaseEntity
    {
        public string StatusName { get; set; }
        public string Description { get; set; }
    }
}
