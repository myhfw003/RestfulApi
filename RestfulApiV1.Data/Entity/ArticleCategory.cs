﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestfulApiV1.Data.Entity
{
    public class ArticleCategory:BaseEntity
    {
        public string CategoryName { get; set; }
        public string Description { get; set; }
        public int? ParentCategoryId { get; set; }
        public virtual ArticleCategory ParentCategory { get; set; }
        public virtual ICollection<ArticleCategory> Children { get; set; }
    }
}
