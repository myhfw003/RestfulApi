﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RestfulApiV1.Data.Entity;
using RestfulApiV1.Domain.DTOs;
using RestfulApiV1.Implement.Paged;
using RestfulApiV1.Interface.Data;
using RestfulApiV1.Utils.Json;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RestfulApiV1.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ArticleCategoryController : ControllerBase
    {
        private readonly IRespository<ArticleCategory> _articleCategoryRep;

        public ArticleCategoryController(IRespository<ArticleCategory> articleCategoryRep)
        {
            _articleCategoryRep = articleCategoryRep;
        }


        // GET: api/<ArticleCategoryController>
        [HttpGet]
        public string Get(int pageIndex = 1, int pageSize = 10)
        {
            var list = _articleCategoryRep.Table.Where(x => x.IsActived == true && x.IsDeleted == false).Include(x=>x.ParentCategory);

            var res = new
            {
                code = 1000,
                data = GetDynamic(new Pagination<ArticleCategory>(list, pageIndex, pageSize)),
                msg = "获取文章分类列表成功"
            };

            return JsonHelper.SerializeObject(res);
        }

        [HttpGet,Route("{id}/forselect")]
        public string GetForSelect(int id)
        {
            var list = _articleCategoryRep.Table
                .Where(x => x.IsActived == true && x.IsDeleted == false && x.Id != id)
                .Select(x=>new {x.Id,x.CategoryName })
                .ToList();

            var res = new
            {
                code = 1000,
                data = list,
                msg = "获取文章分类列表成功"
            };

            return JsonHelper.SerializeObject(res);
        }

        // GET api/<ArticleCategoryController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            var item = _articleCategoryRep.GetById(id);
            var res = new
            {
                code = 1000,
                data = GetDynamic(item),
                msg = "获取文章分类成功"
            };
            return JsonHelper.SerializeObject(res);
        }

        // POST api/<ArticleCategoryController>
        [HttpPost]
        public string Post([FromBody] ArticleCategoryCreation item)
        {
            var parentId =string.IsNullOrEmpty( item.ParentCategoryId) ? null : (int?)int.Parse(item.ParentCategoryId);
            var category = new ArticleCategory
            {
                CategoryName = item.CategoryName,
                Description = item.Description,
                Remarks = item.Remarks,
                ParentCategoryId = parentId
            };
            _articleCategoryRep.Add(category);
            var cate = _articleCategoryRep.Table
                .Where(x => x.Id == category.Id)
                .Include(x => x.ParentCategory).FirstOrDefault();
            var res = new
            {
                code = 1000,
                data = GetDynamic(cate),
                msg = "增加文章分类成功"
            };
            return JsonHelper.SerializeObject(res);
        }

        // PUT api/<ArticleCategoryController>/5
        [HttpPut("{id}")]
        public string Put(int id, [FromBody] ArticleCategoryUpdate value)
        {
            var item = _articleCategoryRep.GetById(id);
            var parentId = string.IsNullOrEmpty(value.ParentCategoryId) ? null : (int?)int.Parse(value.ParentCategoryId);
            dynamic res;
            if (item != null)
            {
                item.CategoryName = value.CategoryName;
                item.Description = value.Description;
                item.ParentCategoryId = parentId;
                _articleCategoryRep.Update(item);
                var other = _articleCategoryRep.Table
                .Where(x => x.IsActived == true && x.IsDeleted == false && x.Id == id)
                .Include(x => x.ParentCategory)
                .FirstOrDefault();
                res = new
                {
                    code = 1000,
                    data = GetDynamic(other),
                    msg = "修改文章分类成功"
                };
            }
            else
            {
                res = new
                {
                    code = 1000,
                    data = "",
                    msg = "当前文章分类不存在"
                };
            }
            return JsonHelper.SerializeObject(res);
        
        }

        // DELETE api/<ArticleCategoryController>/5
        [HttpDelete("{id}")]
        public string Delete(int id)
        {
            var item = _articleCategoryRep.GetById(id);
            dynamic res;
            if (item != null)
            {
                item.IsDeleted = true;
                _articleCategoryRep.Update(item);
                res = new
                {
                    code = 1000,
                    data = "",
                    msg = "删除文章分类成功"
                };
            }
            else
            {
                res = new
                {
                    code = 1000,
                    data = "",
                    msg = "当前文章分类不存在"
                };
            }
            return JsonHelper.SerializeObject(res);
        }

        [HttpPost, Route("{id}/status")]
        public string UpdateCategoryStatus(int id)
        {
            var item = _articleCategoryRep.GetById(id);
            dynamic res;

            if (item != null)
            {
                item.IsActived = !item.IsActived;
                _articleCategoryRep.Update(item);

                res = new
                {
                    Code = 1000,
                    Data = GetDynamic(item),
                    Msg = "修改分类状态成功"
                };
            }
            else
            {
                res = new
                {
                    Code = 1002,
                    Data = "",
                    Msg = "指定分类不存在"
                };
            }


            return JsonHelper.SerializeObject(res);
        }

        private dynamic GetDynamic(ArticleCategory category)
        {
            //var item = _articleCategoryRep.Table.Include(x => x.ParentCategory).FirstOrDefault();
            var parentCategory = category.ParentCategory;
            var res = new
            {
                category.Id,
                category.CategoryName,
                category.Description,
                category.Remarks,
                IsActived = category.IsActived ? 1 : 0,
                category.ParentCategoryId,
                ParentCategoryName= parentCategory != null? parentCategory.CategoryName:""
            };
            return res;
        }

        private dynamic GetDynamic(Pagination<ArticleCategory> categories)
        {
            var list = new List<dynamic>();
            foreach(var category in categories.List)
            {
                var temp = GetDynamic(category);
                list.Add(temp);
            }
            var data = new
            {
                categories.PageIndex,
                categories.PageSize,
                categories.TotalItem,
                categories.TotalPage,
                List = list
            };
            return data;
        }
    }
}
