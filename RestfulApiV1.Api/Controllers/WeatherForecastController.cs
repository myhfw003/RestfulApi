﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using RestfulApiV1.Data.Entity;
using RestfulApiV1.Interface.Data;
using RestfulApiV1.Utils.Json;

namespace RestfulApiV1.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        private readonly IRespository<Users> _userResPository;

        public WeatherForecastController(ILogger<WeatherForecastController> logger, IRespository<Users> userResPository)
        {
            _logger = logger;
            _userResPository = userResPository;
        }

        [HttpGet]
        public string Get()
        {
            //var rng = new Random();
            //return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            //{
            //    Date = DateTime.Now.AddDays(index),
            //    TemperatureC = rng.Next(-20, 55),
            //    Summary = Summaries[rng.Next(Summaries.Length)]
            //})
            //.ToArray();

            var list = _userResPository.Table.ToList();

            return JsonHelper.SerializeObject(list);
        }
    }
}
