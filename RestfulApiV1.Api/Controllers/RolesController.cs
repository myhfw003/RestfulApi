﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using RestfulApiV1.Data.Entity;
using RestfulApiV1.Data.Shared;
using RestfulApiV1.Interface.Data;
using RestfulApiV1.Utils.Json;

namespace RestfulApiV1.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RolesController : ControllerBase
    {
        private readonly IRespository<Roles> _rolesRespository;
        private readonly IRespository<RolePermission> _rolePermissionRespository;

        public RolesController(IRespository<Roles> rolesRespository, IRespository<RolePermission> rolePermissionRespository)
        {
            _rolesRespository = rolesRespository;
            _rolePermissionRespository = rolePermissionRespository;
        }

        [HttpGet]
        public string Get()
        {
            var list = _rolesRespository.Table
                .Where(x => x.IsActived == true && x.IsDeleted == false)
                .ToList();

            var resList = GetDynamic(list);

            var res = new { code = 1000, data = resList, msg = "请求角色列表成功" };
            return JsonHelper.SerializeObject(res);
        }

        [HttpGet("{id}")]
        public string Get(int id)
        {
            var item = _rolesRespository.GetById(id);
            var res = new { code = 1000, data = GetDynamic(item), msg = "请求角色成功" };
            return JsonHelper.SerializeObject(res);

        }

        [HttpPost]
        public string Post([FromBody]Roles role)
        {
            var item = new Roles
            {
                RoleName = role.RoleName,
                Description = role.Description,
                Remarks = role.Remarks
            };
            _rolesRespository.Add(item);

            var res = new
            {
                code = 1000,
                data = GetDynamic(item),
                msg = "增加权限成功"
            };
            return JsonHelper.SerializeObject(res);
        }

        [HttpPut("{id}")]
        public string Put(int id,Roles role)
        {
            var item = _rolesRespository.GetById(id);
            dynamic res;
            if (item != null)
            {
                item.RoleName = role.RoleName;
                item.Description = role.Description;
                _rolesRespository.Update(item);
                res = new { code = 1000, data = GetDynamic(item), msg = "修改角色成功" };
                return JsonHelper.SerializeObject(res);
            }
            else
            {
                res = new { code = 1000, data = "", msg = "当前角色不存在" };
            }
            
            return JsonHelper.SerializeObject(res);
        }

        [HttpDelete("{id}")]
        public string Delete(int id)
        {
            var item = _rolesRespository.GetById(id);
            dynamic res;
            if (item != null)
            {
                item.IsDeleted = true;
                _rolesRespository.Update(item);
                res = new { code = 1000, data = "", msg = "删除角色成功" };
            }
            else
            {
                res = new { code = 1000, data = "", msg = "当前角色不存在" };
            }
            
            return JsonHelper.SerializeObject(res);
        }

        [HttpPost,Route("{id}/permission")]
        public string SaveRolePermission(int id,[FromBody]IEnumerable<PermissionDto> reqList)
        {
            var list = _rolePermissionRespository.Table.Where(x => x.RolesId == id).ToList();
            _rolePermissionRespository.Delete(list);

            var rolePermissionList = new List<RolePermission>();
            foreach(var item in reqList)
            {
                rolePermissionList.Add(new RolePermission
                {
                    RolesId = id,
                    PermissionName = item.Name
                });
            }

            _rolePermissionRespository.Add(rolePermissionList);

            var role = _rolesRespository.GetById(id);

            var resData = new
            {
                role.Id,
                role.RoleName,
                role.Description,
                role.Remarks,
                access = reqList.Select(x => x.Name).ToList()
            };


            var res = new { code = 1000, data = resData, msg = "角色权限保存成功" };
            return JsonHelper.SerializeObject(res);
        }

        private dynamic GetDynamic(Roles role)
        {
            var rolePermission = _rolePermissionRespository.Table
                .Where(x => x.RolesId == role.Id)
                .Select(x => x.PermissionName)
                .ToList();

            return new
            {
                role.Id,
                role.RoleName,
                role.Description,
                role.Remarks,
                Access = rolePermission
            };
        }

        private dynamic GetDynamic(IEnumerable<Roles> roles)
        {
            var rolePermission = _rolePermissionRespository.Table
                .Where(x => roles.Select(t=>t.Id).Contains(x.RolesId))
                .Select(x=>new { x.Id,x.RolesId,x.PermissionName})
                .ToList();

            var resData = new List<dynamic>();
            foreach(var role in roles)
            {
                resData.Add(new
                {
                    role.Id,
                    role.RoleName,
                    role.Description,
                    role.Remarks,
                    Access = rolePermission.Where(x => x.RolesId == role.Id).Select(x => x.PermissionName).ToList()
                });
            }

            return resData;
        }
    }
}
