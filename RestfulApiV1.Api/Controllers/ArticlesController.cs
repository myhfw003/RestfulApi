﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RestfulApiV1.Data.Entity;
using RestfulApiV1.Domain.DTOs;
using RestfulApiV1.Implement.Paged;
using RestfulApiV1.Interface.Data;
using RestfulApiV1.Utils.Json;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RestfulApiV1.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ArticlesController : ControllerBase
    {
        private readonly IRespository<Articles> _articleRes;
        private readonly IRespository<ArticleStatus> _articleStatusRes;

        public ArticlesController(IRespository<Articles> articleRes, IRespository<ArticleStatus> articleStatusRes)
        {
            _articleRes = articleRes;
            _articleStatusRes = articleStatusRes;
        }


        // GET: api/<ArticlesController>
        [HttpGet]
        public string Get(int pageIndex = 1, int pageSize = 10)
        {
            var list = _articleRes.Table
                .Include(x => x.ArticleCategory)
                .Where(x => x.IsActived == true && x.IsDeleted == false);

            var res = new
            {
                code = 1000,
                data = GetDynamic( new Pagination<Articles>(list, pageIndex, pageSize)),
                msg = "获取文章列表成功"
            };

            return JsonHelper.SerializeObject(res);
        }

        // GET api/<ArticlesController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            var item = _articleRes.Table
                .Where(x => x.IsDeleted == false && x.IsActived == true && x.Id == id).FirstOrDefault();
            
            var res = new
            {
                code = 1000,
                data = item,
                msg = "获取文章成功"
            };

            return JsonHelper.SerializeObject(res);
        }

        // POST api/<ArticlesController>
        [HttpPost]
        public string Post([FromBody] ArticleCreation value)
        {
            var status = _articleStatusRes.Table
                .Where(x => x.IsActived == true && x.IsDeleted == false && x.StatusName == "待审核").FirstOrDefault();
            var item = new Articles
            {
                Title = value.Title,
                ArticleContent = value.ArticleContent,
                CoverImgUrl=value.CoverImgUrl,
                ArticleStatusId = status.Id,
                ArticleCategoryId = value.ArticleCategoryId
            };

            _articleRes.Add(item);

            var temp = _articleRes.Table
                .Where(x => x.IsActived == true && x.IsDeleted == false && x.Id == item.Id)
                .Include(x => x.ArticleCategory)
                .FirstOrDefault();

            var res = new
            {
                code = 1000,
                data = GetDynamic(temp),
                msg = "新增文章成功"
            };

            return JsonHelper.SerializeObject(res);
        }

        // PUT api/<ArticlesController>/5
        [HttpPut("{id}")]
        public string Put(int id, [FromBody] ArticleCreation value)
        {
            var item = _articleRes.GetById(id);
            dynamic res;
            if (item != null)
            {
                item.Title = value.Title;
                item.ArticleContent = value.ArticleContent;
                item.CoverImgUrl = value.CoverImgUrl;
                item.ArticleStatusId = value.ArticleStatusId;
                item.ArticleCategoryId = value.ArticleCategoryId;
                _articleRes.Update(item);
                var temp = _articleRes.Table
                .Where(x => x.IsActived == true && x.IsDeleted == false && x.Id == item.Id)
                .Include(x => x.ArticleCategory)
                .FirstOrDefault();
                res = new
                {
                    code = 1000,
                    data = GetDynamic(temp),
                    msg = "修改文章成功"
                };
            }
            else
            {
                res = new
                {
                    code = 1002,
                    data = item,
                    msg = "当前文章不存在"
                };
            }
            return JsonHelper.SerializeObject(res);
        }

        // DELETE api/<ArticlesController>/5
        [HttpDelete("{id}")]
        public string Delete(int id)
        {
            var item = _articleRes.GetById(id);
            dynamic res;
            if (item != null)
            {
                item.IsDeleted = true;
                _articleRes.Update(item);
                res = new
                {
                    code = 1000,
                    data = item,
                    msg = "删除文章成功"
                };
            }
            else
            {
                res = new
                {
                    code = 1002,
                    data = item,
                    msg = "当前文章不存在"
                };
            }
            return JsonHelper.SerializeObject(res);
        }

        private dynamic GetDynamic(Articles articles)
        {
            var res = new
            {
                articles.Id,
                articles.Title,
                ArticleContent=string.IsNullOrEmpty(articles.ArticleContent)?"":articles.ArticleContent,
                CoverImg=articles.CoverImgUrl,
                articles.ArticleStatusId,
                articles.ArticleCategoryId,
                articles.ArticleCategory.CategoryName,
                articles.Remarks
            };

            return res;
        }

        private dynamic GetDynamic(Pagination<Articles> categories)
        {
            var list = new List<dynamic>();
            foreach (var category in categories.List)
            {
                var temp = GetDynamic(category);
                list.Add(temp);
            }
            var data = new
            {
                categories.PageIndex,
                categories.PageSize,
                categories.TotalItem,
                categories.TotalPage,
                List = list
            };
            return data;
        }
    }
}
